extends Area2D

signal score_points(points)
signal expired

@export var max_xp = 45
var xp_remaining

func _ready():
	xp_remaining = max_xp;

func _on_tick_timer_timeout():
	var count = 0;
	var bodies = self.get_overlapping_bodies()
	for body in bodies:
		if body.is_in_group("critter"):
			count += 1;
	
	if count > 5:
		score(5);
	else:
		score(count);
	
func score(points):
	if points < xp_remaining:
		xp_remaining -= points;
		score_points.emit(points);
	else:
		var leftover = xp_remaining;
		xp_remaining = 0;
		score_points.emit(leftover);
		remove();

func remove():
	expired.emit();
	queue_free();
	
	
func _draw():
	var center = Vector2(0, 0)
	var radius = 120
	var color = Color(0.0, 0.7, 0.6, 0.2)
	draw_circle(center, radius, color)


func _on_move_zone_timer_timeout():
	remove();
