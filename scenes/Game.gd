extends Node2D

@export var screen_width = 1280;
@export var screen_height = 800;

var xp_zone_scene = preload("res://scenes/xp_zone.tscn")
var rng
var spawn_zone_side = 0;

func _ready():
	rng = RandomNumberGenerator.new();
	spawn_random_xp_zone();

func _process(delta):
	var time_remaining = floor($GameEndTimer.time_left);
	$UI/TopLeftPanel/VBoxContainer/TimeRemainingLabel.text = "Time Remaining: " + str(time_remaining) + "s"

func _on_game_end_timer_timeout():
	get_tree().change_scene_to_packed(load('res://scenes/LooseScreen.tscn'));

func _on_defender_xp_increase(xp):
	$UI/ColorRect/VBoxContainer/DefenderXPLabel.text = "Defender XP: " + str(xp)


func spawn_xp_zone(x, y):
	var instance = xp_zone_scene.instantiate();
	instance.position = Vector2(x, y);
	instance.score_points.connect($SwarmPoint.get_xp)
	instance.expired.connect(_on_xp_zone_expired);
	add_child(instance);


func spawn_random_xp_zone():
	var x = 0;
	var y = rng.randi_range(150, (screen_height - 150));
	var buffer = 200;
	if spawn_zone_side == 0:
		# Spawn in the left of the screen
		x = rng.randi_range(buffer, ((screen_width / 2) - buffer))
		spawn_zone_side = 1;
	else:
		# Spawn in the right of the screen
		x = rng.randi_range(((screen_width / 2) + buffer), (screen_width - buffer))
		spawn_zone_side = 0;
	
	spawn_xp_zone(x, y)

func _on_swarm_point_update_xp(xp):
	$UI/ColorRect/VBoxContainer/SwarmXPLabel.text = "Swarm XP: " + str(xp)

func _on_xp_zone_expired():
	spawn_random_xp_zone();
