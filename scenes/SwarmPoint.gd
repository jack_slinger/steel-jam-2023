extends RigidBody2D

signal update_xp(xp)

@export var speed = 300.0
var xp = 0;

func _physics_process(delta):
	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	
	var x_direction = Input.get_axis("ui_left", "ui_right")
	if x_direction:
		apply_central_force(Vector2(25000.0 * x_direction, 0) * delta)
	
	var y_direction = Input.get_axis("ui_up", "ui_down")
	if y_direction:
		apply_central_force(Vector2(0, 25000.0 * y_direction) * delta)

func _integrate_forces(body_state):
	body_state.linear_velocity = body_state.linear_velocity.move_toward(Vector2(0,0), (body_state.linear_velocity.length() * body_state.step ))
	


func _on_body_entered(body):
	print("Collision!");
	print(body);

func get_xp(xp_gained):
	xp += xp_gained;
	update_xp.emit(xp);
