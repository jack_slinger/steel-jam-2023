extends Node

var dps_scene = preload("res://scenes/dps.tscn");
var healer_scene = preload("res://scenes/healer.tscn");
var tank_scene = preload("res://scenes/tank.tscn");

@export var screen_width = 1280;
@export var screen_height = 800;
@export var dps_ratio = 100;
@export var tank_ratio = 0;
@export var healer_ratio = 0;
var base_spawn_time;
var rng

# Called when the node enters the scene tree for the first time.
func _ready():
	base_spawn_time = $SpawnTimer.wait_time;
	rng = RandomNumberGenerator.new();


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func spawn_critter(x, y):
	var critter_type = rng.randf_range(0, (dps_ratio + tank_ratio + healer_ratio));
	var instance;
	
	if critter_type <= dps_ratio:
		instance = instantiate_dps();
	elif critter_type <= dps_ratio + tank_ratio:
		instance = instantiate_tank();
	elif critter_type <= dps_ratio + tank_ratio + healer_ratio:
		instance = instantiate_healer();
	else:
		pass
	
	instance.position.x = x;
	instance.position.y = y;
	add_child(instance)

func instantiate_dps():
	return dps_scene.instantiate();

func instantiate_tank():
	var instance = tank_scene.instantiate();
	instance.max_health = 2;
	return instance;

func instantiate_healer():
	var instance = healer_scene.instantiate();
	#instance.rotates = false;
	return instance;


func spawn_at_top():
	spawn_critter(rng.randi_range(0, screen_width), 0);

func spawn_at_right():
	spawn_critter(screen_width, rng.randi_range(0, screen_height));

func spawn_at_bottom():
	spawn_critter(rng.randi_range(0, screen_width), screen_height);

func spawn_at_left():
	spawn_critter(0, rng.randi_range(0, screen_height));

func _on_spawn_timer_timeout():
	var spawn_pointer_pos = get_parent().get_node("SwarmPoint").position;
	var top_or_side = rng.randi_range(0, 1);
	if top_or_side == 0:
		# Bias top or bottom based on spawn pointer position
		if rng.randf_range(0, screen_height) > spawn_pointer_pos.y:
			spawn_at_top();
		else:
			spawn_at_bottom();
	else:
		# Bias left or right base on spawn pointer position
		if rng.randf_range(0, screen_width) > spawn_pointer_pos.x:
			spawn_at_left();
		else:
			spawn_at_right();


func _on_swarm_point_update_xp(xp):
	var level = xp / 30;
	
	var wait_time = base_spawn_time;
	if level == 0:
		wait_time = 2;
	elif level == 1:
		wait_time = 1.82;
		tank_ratio = 10;
	elif level == 2:
		wait_time = 1.7;
		tank_ratio = 12;
		healer_ratio = 2;
	elif level == 3:
		wait_time = 1.5;
		tank_ratio = 15;
		healer_ratio = 3;
	elif level == 4:
		wait_time = 1.2;
		tank_ratio = 18;
		healer_ratio = 4;
	elif level == 5:
		wait_time = 1.0;
		tank_ratio = 22;
		healer_ratio = 5;
	elif level == 6:
		wait_time = 0.8;
		tank_ratio = 24;
		healer_ratio = 6;
	elif level == 7:
		wait_time = 0.7;
		tank_ratio = 27;
		healer_ratio = 7;
	else:
		wait_time = 0.6 - ((level - 7) * 0.1)
		tank_ratio = 15;
		healer_ratio = 7;
	
	$SpawnTimer.wait_time = wait_time;
