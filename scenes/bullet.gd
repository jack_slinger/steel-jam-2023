extends Node2D

@export var direction = Vector2.ZERO
@export var speed = 1000

signal hit_critter

var velocity = Vector2.ZERO


func _ready():
	rotation = direction.angle() + deg_to_rad(90);

func _physics_process(delta):
	velocity = direction * speed
	position += velocity * delta


func _on_expiration_timer_timeout():
	self.queue_free();


func _on_detection_area_entered(area):
	if area.is_in_group("shield"):
		area.shield_hit();
		self.queue_free();


func _on_detection_body_entered(body):
	if !body.is_in_group("bullet_immune"):
		hit_critter.emit();
		body.take_damage();
		self.queue_free();
