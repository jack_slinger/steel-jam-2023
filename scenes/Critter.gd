extends CharacterBody2D

var death_scene = preload("res://scenes/death_particles.tscn")

@export var base_speed = 100.0
@export var speed_variance = 10
@export var steer_force = 75.0
@export var rotates = true;
@export var max_health = 1;

var rng
var acceleration = Vector2.ZERO
var speed = base_speed
var target
var health

func _ready():
	rng = RandomNumberGenerator.new();
	# TODO: Is there a nicer way to reference the point here
	target = get_parent().get_parent().get_node("SwarmPoint");
	speed = base_speed + rng.randf_range(-speed_variance, speed_variance);
	health = max_health;

func _physics_process(delta):
	acceleration += seek();
	velocity += acceleration * delta;
	velocity = velocity.limit_length(speed);
	
	if rotates:
		rotation = velocity.angle();

	move_and_slide();

func seek():
	var steer = Vector2.ZERO
	if target:
		var desired = (target.position - position).normalized() * speed
		steer = (desired - velocity).normalized() * steer_force
	return steer

func take_damage():
	health -= 1;
	if health <= 0:
		var instance = death_scene.instantiate()
		instance.position = self.position
		get_parent().add_child(instance)
		
		queue_free()


func _on_shield_body_entered(body):
	pass;
	#print("Shield!");
	#print(body);
	#if body.is_in_group("bullet"):
		#print("Bullet!!!")
		#body.velocity = Vector2.ZERO;
		#body.queue_free();
