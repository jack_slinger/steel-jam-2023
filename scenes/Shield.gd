extends Area2D

func shield_hit():
	$CollisionShape2D.disabled = true;
	$ShieldParticles.emitting = false
	self.remove_from_group("shield");
	$Sprite2D.hide();
	$RechargeTimer.start();


func _on_recharge_timer_timeout():
	$CollisionShape2D.disabled = false;
	$ShieldParticles.emitting = true
	self.add_to_group("shield");
	$Sprite2D.show();
