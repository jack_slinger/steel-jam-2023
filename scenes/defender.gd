extends CharacterBody2D

signal xp_increase(new_xp)

@export var rotation_speed = 5.5;
@export var xp_for_rotation_speed = 17;
@export var xp_for_gun_increase = 7;
@export var extra_xp_needed_per_level = 7;
var base_firing_rate

var bullet = preload("res://scenes/bullet.tscn");
var rng
var target
var xp = 0;
var rotation_level = 0;
var gun_level = 0;
var bullet_level = 0;
var fire_speed_level = 0;

func _ready():
	rng = RandomNumberGenerator.new();
	base_firing_rate = $ShootTimer.wait_time;

func _process(delta):
	pass


func _physics_process(delta):
	find_nearest_critter();
	if target:
		var target_rotation = (target.position - position).normalized().angle();
		rotation = move_toward(rotation, target_rotation, (rotation_speed * delta));

func aim_at(target):
	rotation = (target.position - position).normalized().angle()


func find_nearest_critter():
	var closest;
	var best_distance = 999999999;
	for critter in get_tree().get_nodes_in_group("critter"):
		var distance = global_transform.origin.distance_to(critter.global_transform.origin);
		if distance < best_distance:
			best_distance = distance;
			closest = critter;
	
	target = closest;

func shoot(direction):
	var instance = bullet.instantiate();
	instance.direction = direction;
	instance.position.x = position.x;
	instance.position.y = position.y;
	instance.hit_critter.connect(_on_bullet_hit);
	get_parent().add_child(instance);


func _on_shoot_timer_timeout():
	var pointing_at = Vector2(1, 0).rotated(self.rotation).normalized();
	
	if bullet_level == 0:
		shoot(pointing_at);		
	elif bullet_level == 1:
		shoot(pointing_at.rotated(deg_to_rad(1)));
		shoot(pointing_at.rotated(deg_to_rad(-1)));
	elif bullet_level == 2:
		shoot(pointing_at);
		shoot(pointing_at.rotated(deg_to_rad(3)));
		shoot(pointing_at.rotated(deg_to_rad(-3)));
	elif bullet_level == 3:
		shoot(pointing_at.rotated(deg_to_rad(1)));
		shoot(pointing_at.rotated(deg_to_rad(-1)));
		shoot(pointing_at.rotated(deg_to_rad(3)));
		shoot(pointing_at.rotated(deg_to_rad(-3)));
	else:
		shoot(pointing_at);
		shoot(pointing_at.rotated(deg_to_rad(3)));
		shoot(pointing_at.rotated(deg_to_rad(-3)));
		shoot(pointing_at.rotated(deg_to_rad(5)));
		shoot(pointing_at.rotated(deg_to_rad(-5)));

func _on_area_2d_body_entered(body):
	if body.is_in_group("critter"):
		get_tree().change_scene_to_packed(load('res://scenes/WinScreen.tscn'));

# TODO: Change this to be when critters die
func _on_bullet_hit():
	xp += 1;
	xp_increase.emit(xp);
	
	if (xp - (rotation_level * xp_for_rotation_speed)) > xp_for_rotation_speed:
		rotation_level += 1;
		rotation_speed = rotation_speed * 1.1;

	var xp_for_current_levels = 0;
	for level in gun_level:
		xp_for_current_levels += xp_for_gun_increase + (extra_xp_needed_per_level * level)
	
	var xp_diff = xp - xp_for_current_levels;
	if xp_diff >= xp_for_gun_increase + (extra_xp_needed_per_level * gun_level):
		print("Level up");
		print(xp);
		gun_level += 1;
		
		if gun_level % 2 == 0:
			bullet_level += 1;
		else:
			fire_speed_level += 1;

	var wait_time = 0.5;
	if fire_speed_level == 0:
		wait_time = 0.5;
	elif fire_speed_level == 1:
		wait_time = 0.4;
	elif fire_speed_level == 2:
		wait_time = 0.3;
	elif fire_speed_level == 3:
		wait_time = 0.275;
	elif fire_speed_level == 4:
		wait_time = 0.25;
	else:
		wait_time = 0.2;
	
	$ShootTimer.wait_time = wait_time;
